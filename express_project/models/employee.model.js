const mongoose = require('mongoose');
var employeeScema = new mongoose.Schema({
  fullname:{
    type:String,
    required:'field is mandatory'
  },
  email:{
    type:String
  },
  mobile:{
    type:String
  },
  city:{
    type:String
  }
});

mongoose.model('Employee',employeeScema);