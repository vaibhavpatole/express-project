const express = require('express');
const mangoose = require('mongoose');
const Employee = mangoose.model('Employee');

var router = express.Router();

router.get('/',(req,res)=>{
    res.render("employee/addOrEdit",{title:"insert Employee"});
});

router.post('/',(req,res)=>{
    insertRecord(req,res);
});

function insertRecord(req,res){
    var employee = new Employee();
    employee.fullname = req.body.fullname;
    employee.email = req.body.email;
    employee.mobile = req.body.mobile;
    employee.city = req.body.city;
    employee.save((err,doc)=>{
        if(!err)
            res.redirect('employee/list');
        else
            console.log("error" +err);
    });
}

router.get('/list',(req,res)=>{
    Employee.find((err,docs)=>{
        if(!err){
            res.render("employee/list",{
                list:docs
            });
        }
        else{
            console.log("error"+err);
        }
    });
});

router.get('/:id',(req,res)=>{
    Employee.findById(req.params.id,(err,doc)=>{
        if(!err){
            res.render("employee/addOrEdit",{title:"update Employee",employee:doc});
        }
    });
});
module.exports = router;