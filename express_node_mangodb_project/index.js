require('./modules/db')
const express = require('express')
const path = require('path')
const exphbs = require('express-handlebars')
const userController = require('./controllers/userController')


const app = express()
app.set("view engine", "html");

//to start the server and execute the root get function
app.listen(3000, () => console.log('App open on port http://localhost:3000/user'))

app.use('/user',userController);

app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');