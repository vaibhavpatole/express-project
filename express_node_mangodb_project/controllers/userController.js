
const express = require('express');
var router = express.Router();
var path = require('path');
const stringify = require('json-stringify-safe');
const bodyparser = require('body-parser')
const mongoose = require('mongoose')
const User = mongoose.model('User')


router.use(bodyparser.urlencoded({
  extended: true
}));
router.use(bodyparser.json());


//application routes
router.get('/', function (req, res) { 
  //res.send("hii");
  res.sendFile(path.resolve('views/mainLayout.html'));
})

router.get('/index',function(req,res){
  res.sendFile(path.resolve('views/index.html'));
  })
  
router.get('/list',function(req,res){
  User.find((err,docs)=>{
    if(!err){
      res.send(docs);
    }
    else{
      console.log("error message"+err);
    }
  });
})
  
  router.post('/get_data',function(req,res) {
    // response = {  
    //   Name:req.query.user_name,  
    //   Email:req.query.user_email,
    //   Password:req.query.user_password  
    // };  
    // console.log(response);  
    // res.end(JSON.stringify(response));
    // res.send('<p>Name : ' + req.query['user_name']+'</p><p>Email Id : '+req.query['user_email']+'</p><p>Password : '+req.query['user_password']);
    // res.send(req.body);
    insertRecord(req,res);
  })
  
  function insertRecord(req,res){
    var user = new User();
    user.name = req.body.user_name;
    user.email = req.body.user_email;
    user.password = req.body.user_password;
    user.save((err,doc)=>{
      if(!err){
        res.redirect('list');
        console.log("data inserted");
      }  
      else
        console.log("error occurerd in " +err);
    });
  }

  module.exports = router;